#!/bin/sh
# This scripts calls update_test_binaries in a loop
# passing it each line to test-repositories.txt

RELEASE=$1
[ -z "$RELEASE" ] && echo "Needs a release" && exit 1
BRANCH=$2
[ -z "$BRANCH" ] && echo "Needs a branch" && exit 1

cat test-repositories.txt | while read repository; do
	./update_test_binaries.sh -r ${repository} -l ${RELEASE} -b ${BRANCH}
done
