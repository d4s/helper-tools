#!/bin/bash

set -eu

pwd=`dirname $0`
pwd=`cd $pwd ; /bin/pwd`

usage()
{
	echo "$0 -b <branch> -r <release> [-p]

Creates the branches for each release in all tests projects
All operations are done in a temporary directory

Options:
    -b, --branch: Branch to use as a source for the new branches (usually master)
    -r, --release: release to use as the name of target branches (12.34)
    -p, --push: Push after update, the default is not to push

For example: branch-tool.sh -b master -r 18.06 -p
"
	exit 1
}

BRANCH=""
RELEASE=""
PUSH=0
CLEAN=0
while [ $# -gt 0 ] ; do
	case $1 in
	-b|--branch) shift; BRANCH=$1 ;;
	-r|--release) shift; RELEASE=$1 ;;
	-p|--push) PUSH=1 ;;
	*) usage;;
	esac
	shift
done

WORKDIR=`mktemp -d`

[ -z "${RELEASE}" ] && usage
[ -z "${BRANCH}" ] && usage

echo "Initial branch: ${BRANCH}"
echo "Release: ${RELEASE}"
echo "Push: ${PUSH}"
echo "Workdir: ${WORKDIR}"

cd ${WORKDIR}
cat $pwd/test-repositories.txt | while read REPO ; do
	echo "Processing ${REPO}"
	git clone git@gitlab.apertis.org:tests/${REPO}.git
	
	git -C ${REPO} checkout -b "${RELEASE}" "${BRANCH}" 
done

cd ${WORKDIR}
if [ "${PUSH}" = "1" ] ; then
	cat $pwd/test-repositories.txt | while read REPO ; do
		echo "Pushing ${REPO}"
		git -C ${REPO} push origin ${RELEASE}:${RELEASE}
	done
fi

cd ${WORKDIR}
cat $pwd/test-repositories.txt | while read REPO ; do
	[ -d ${REPO}/.git ] && rm -rf ${WORKDIR}/${REPO}
done
cd $pwd; rmdir ${WORKDIR}

